# Gyms on map
Shows a static map with specified gyms on map. Contains only html and js, does not need to be on a server to work.

## Usage
1. Edit gyms.js (add locations)
2. Edit index.html (change page title and start location)
3. Open index.html with browser and see the gyms

## JavaScript used
### Leaflet:
https://github.com/Leaflet/Leaflet/

### Leaflet.markercluster:
https://github.com/Leaflet/Leaflet.markercluster/


## Images
Crosshair image:
https://openclipart.org/detail/239663/gun-sight

Marker icons:
https://github.com/pointhi/leaflet-color-markers


## Licenses
### Custom code:
MIT

### Leaflet:
https://github.com/Leaflet/Leaflet/blob/master/LICENSE


### Leaflet.markercluster:
MIT
https://github.com/Leaflet/Leaflet.markercluster/blob/master/MIT-LICENCE.txt


### Crosshair image:
Creative Commons Zero 1.0 Public Domain License 
https://openclipart.org/share


### Marker icons:
BSD 2-Clause License

img/LICENSE

or URL:

https://github.com/pointhi/leaflet-color-markers/blob/master/LICENSE

### Portals
https://xgress.com/portal_search