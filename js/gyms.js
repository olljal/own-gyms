// Enter gyms here:
// Name, level, lat, lon
const jyms=[
      [
        "\"Exegi monumentum\" /Horatius/",
        "basic",
        60.919431,
        23.362749
      ],
      [
        "Aarnkari",
        "silver",
        61.144302,
        21.460964
      ],
      [
        "ABC",
        "basic",
        61.136592,
        21.487433
      ],
      [
        "Adapted Webbed",
        "bronze",
        39.627022,
        -84.179786
      ],
      [
        "Aikavaellus",
        "silver",
        60.412132,
        22.429672
      ],
      [
        "Aikavaellus - Ensimmäiset Tähdet",
        "bronze",
        60.416147,
        22.438689
      ],
      [
        "Aikavaellus - Fennia orogenia",
        "silver",
        60.441412,
        22.295966
      ],
      [
        "Aikavaellus - Galaksijoukot",
        "silver",
        60.41042,
        22.41300
      ],
      [
        "Aikavaellus - Galaktiset superjoukot",
        "silver",
        60.412346,
        22.353313
      ],
      [
        "Aikavaellus - Karjalan kallioperä",
        "silver",
        60.437603,
        22.302658
      ],
      
      [
        "Aikavaellus - Svekofenninen vuoristo",
        "silver",
        60.442185,
        22.295116
      ],
      [
        "Aikavaellus - 13,7 mrd vuotta",
        "gold",
        60.425838,
        22.346194
      ],
      [
        "Aikavaellus Maailmankaikkeus",
        "silver",
        60.409551,
        22.403649
      ],
      [
        "Aikavaellus Tähtien Synty",
        "gold",
        60.408393,
        22.391555
      ],
      [
        "Aino",
        "bronze",
        60.453435,
        22.254111
      ],
      [
        "Aitta",
        "bronze",
        60.465447,
        22.310375
      ],
      [
        "Ala Lemun Kartano",
        "silver",
        60.39011,
        22.305151
      ],
      [
        "Alnor Stone",
        "silver",
        60.438152,
        22.326617
      ],
      [
        "Alte Zechen Lore",
        "bronze",
        51.616914,
        7.190827
      ],      
      [
        "Ankkalammen Suihkulähde",
        "silver",
        61.128553,
        21.521799
      ],
      [
        "Ankkurit",
        "silver",
        60.873691,
        21.701775
      ],
      [
        "Ankkuri",
        "bronze",
        60.446603,
        22.246982
      ],
      [
        "Anundilanaukion Suihkulähde",
        "bronze",
        61.128210,
        21.507825
      ],
      [
        "Aronahde Frisbeegolf 4",
        "silver",
        61.114238,
        21.509549
      ],
      [
        // Inokashira Dori, 2 Chome Izumi, Suginami, Tokyo 168-0063, Japan
        "Asobiba 90 ban park",
        "bronze",
        35.67368,
        139.648787
      ],
      [
        "Asociación Cultural Andaluza de Esplugues",
        "bronze",
        41.3727712,
        2.0914852
      ],
      [
        "Attilan piippu",
        "bronze",
        61.4987517,
        23.7785681
      ],
      [
        "Aurajoen Pyöräreitti",
        "basic",
        60.560473,
        22.447311
      ],
      [
        "Auranlaakson koiraparkki",
        "basic",
        60.466272,
        22.346674
      ],
      [
        "Auringonnousun Liukumäki",
        "bronze",
        60.412814,
        22.309261
      ],
      [
        "Auvaisbergin leikkipuisto",
        "bronze",
        60.402166,
        22.337803
      ],
      [
        "Berliner Gedenktafel: Friedrich Ebert",
        "bronze",
        52.507081,
        13.471286
      ],
      [
        "Birgittalaisnunna",
        "bronze",
        60.470957,
        22.015183
      ],
      [
        "Bore Ship",
        "basic",
        60.435027,
        22.23334
      ],
      [
        "Buddhist Tempel Of Moisio",
        "basic",
        60.541712,
        22.29777
      ],
      [
        "Campus Aboensis",
        "silver",
        60.454063,
        22.287352
      ],
      [
        "Cantina Window Mural",
        "bronze",
        30.681594,
        -83.219972
      ],
      [
        "Carl Ludviginpuisto",
        "basic",
        60.413989,
        22.275677
      ],
      [
        "Cast Iron Bass",
        "bronze",
        33.412448,
        -86.716324
      ],
      [
        "Church of Jesus Christ of Latter-day Saints Rauma",
        "basic",
        61.11491,
        21.5272879
      ],
      [
        "Clé De La Ville",
        "bronze",
        47.748484,
        7.341914
      ],
      [
        "Cross on A Field",
        "silver",
        60.428741,
        22.312624
      ],
      [
        "Cubos",
        "bronze",
        19.454867,
        -99.147475
      ],
      [
        "Dead On Time",
        "bronze",
        60.455932,
        22.264191
      ],
      [
        "Discgolfpark Kaarina",
        "silver",
        60.414137,
        22.359126
      ],
      [
        "Drontti Edvard",
        "basic",
        60.473141,
        22.0022
      ],
      [
        "Electric Trees",
        "bronze",
        60.470122,
        22.236328
      ],
      [
        "Elämän Lähde",
        "basic",
        60.878297,
        21.687753
      ],
      [
        "Elämän Sana",
        "bronze",
        60.45903,
        22.274093
      ],
      [
        "Emil Cedercreutz - Horse",
        "silver",
        61.126813,
        21.517948
      ],
      [
        "Engine 1167",
        "silver",
        61.132790,
        21.497048
      ],
      [
        "Eskimokodon leikkipuisto",
        "basic",
        60.411689,
        22.305526
      ],
      [
        "Espai Macia",
        "bronze",
        41.521204,
        0.8667053
      ],
      [
        "Espositio",
        "bronze",
        60.441589,
        22.247765
      ],
      [
        /* This is a guess.. Googled */
        "Ethnic saxophone player",
        "bronze",
        -6.2638579,
        106.7818599
      ],
      [
        "Extraterrestrial Meat Eating Plants",
        "silver",
        60.399072,
        22.375163
      ],
      [
        "Fabian Klingendahl Portrait",
        "basic",
        61.4908199,
        23.7529291
      ],
      [
        "Fibonacci Sequence 1–55",
        "bronze",
        60.438883,
        22.23813
      ],
      [
        "Fontaine Perrre Viret",
        "bronze",
        46.5227867,
        6.6340926,
      ],
      [
        "Fountain",
        "bronze",
        60.415344,
        22.289473
      ],
      [
        "Fountain Face",
        "basic",
        61.497961,
        23.762248
      ],
      [
        /* Address: 40 Wilson Street, Strathfield NSW 2135, Australia */
        "Freshwater Park",
        "basic",
        -33.880831,
        151.073441
      ],
      [
        "Fuente Montesa",
        "bronze",
        41.3708576,
        2.0809182
      ],
      [
        "G.A. Petreliuksen Muistomerkki",
        "bronze",
        60.446937,
        22.269374
      ],
      [
        "Genius ohjaa nuoruutta",
        "silver",
        60.45476,
        22.284316
      ],
      [
        "Giant Crab Trap",
        "silver",
        61.136577,
        21.473443
      ],
      [
        "Giant Soda Can",
        "bronze",
        60.878896,
        21.685343
      ],
      [
        "Glad Abbot Pub",
        "bronze",
        52.236606,
        0.687418
      ],
      [
        /* Was: Aryz Turku 2011 - Renamed to Graffiti */
        "Graffiti",
        "silver",
        60.452736,
        22.272345
      ],
      [
        "Graffiti Bahnunterführung",
        "bronze",
        47.5608676,
        8.2163655
        /* Might be wrong.. */
      ],
      [
        "Grand Flaneur Beach Outdoor Gym Circuit Plaque",
        "bronze",
        -33.90634,
        150.95751
      ],
      [
        "Green Seal",
        "silver",
        60.431203,
        22.322286
      ],
      [
        "Growth",
        "basic",
        61.494724,
        23.780338
      ],
      [
        "Gustavus II Adolphus",
        "silver",
        60.45142,
        22.279943
      ],
      [
        "Haliskylä",
        "silver",
        60.470028,
        22.308072
      ],
      [
        "Halisten ja Orikedon Ulkoilureitit",
        "basic",
        60.481759,
        22.303677
      ],
      [
        "Handicrafts Museum",
        "bronze",
        60.446882,
        22.276136
      ],
      [
        "Happy Seal",
        "basic",
        60.43588,
        22.22408
      ],
      [
        "Harmonia",
        "bronze",
        60.439277,
        22.243396
      ],
      [
        "Harvaluodon Uimaranta",
        "basic",
        60.370602,
        22.515418
      ],
      [
        "Helenan Kahvio",
        "basic",
        60.447276,
        22.212011
      ],
      [
        "Henrik Church Bell Tower",
        "bronze",
        60.433219,
        22.2963
      ],
      [
        "Hepokullan seurakuntatalo",
        "basic",
        60.473864,
        22.249994
      ],
      [
        "Heppa",
        "basic",
        60.495661,
        22.345592
      ],
      [
        "Hero Statue",
        "basic",
        61.126005,
        21.519206
      ],
      [
        "Herrasniitynkatu 10 leikkipaikka",
        "bronze",
        60.40329,
        22.32864
      ],
      [
        "Heterocera - The Moth",
        "silver",
        60.436017,
        22.173559
      ],
      [
        "Hintsankujan Leikkikenttä",
        "silver",
        60.440938,
        22.362152
      ],
      [
        "Hirsipuumäki-Galgbacken",
        "silver",
        60.450143,
        22.284288
      ],
      [
        "Hirvittävä Juttu",
        "silver",
        60.434149,
        22.176365
      ],
      [
        "Historiikki",
        "basic",
        60.444266,
        22.357994
      ],
      [
        "Hitsaajat",
        "bronze",
        60.455299,
        22.270782
      ],
      [
        "Honkamäenpuiston Leikkipaikka",
        "bronze",
        60.434972,
        22.294127
      ],
      [
        "Horse With Jockey",
        "bronze",
        52.3297579,
        4.8705048
      ],
      [
        "Hovilanmetsän leikkipuisto",
        "gold",
        60.43777,
        22.36832
      ],
      [
        "Hovirinnan Uimaranta",
        "silver",
        60.396058,
        22.37784
      ],
      [
        "Hovirinnan venesatama",
        "silver",
        60.395104,
        22.37409
      ],
      [
        "Huljun kenttä",
        "basic",
        60.411899,
        22.305403
      ],
      [
        "Hungry Hungry Seal",
        "gold",
        60.436786,
        22.325226
      ],
      [
        "Hurttivuoren koripallokenttä",
        "gold",
        60.438171,
        22.336593
      ],
      [
        "I C U",
        "basic",
        60.412749,
        22.422911
      ],
      [
        "ICT City",
        "bronze",
        60.449584,
        22.295118
      ],
      [
        "Ilmaristen BMX-rata",
        "bronze",
        60.506618,
        22.38101
      ],
      [
        "Inkeriläisten Vainajien Muistomerkki",
        "gold",
        60.435871,
        22.310157
      ],
      [
        "Ispoisten seurakuntakoti",
        "basic",
        60.42658,
        22.26958
      ],
      [
        "Itiö",
        "bronze",
        60.435398,
        22.17164
      ],/*
      [
        joku vihreän teen mesta
        "ITO EN ??",
        "bronze",
        
      ],*/
      [
        "Itsenäinen Suomi",
        "basic",
        60.484923,
        22.172966
      ],
      [ 
        "Itsenäinen Suomi 70 v",
        "silver",
        61.1289782,
        21.5104132
      ],
      [
        "Jaanin vuokraviljelypalstat",
        "bronze",
        60.454594,
        22.334807
      ],
      [
        "Jaaninpuiston leikkipaikka",
        "bronze",
        60.454485,
        22.330889
      ],
      [
        "Jarkko Laine Mural",
        "bronze",
        60.438969,
        22.260485
      ],
      [
        "Jarkko Nieminen Areena",
        "silver",
        60.479894,
        22.262622
      ],
      [
        "Jatulintarha",
        "silver",
        61.141822,
        21.484113
      ],
      [
        /* Rauma, Pyynpää */
        "Jehova's Witnesses Church",
        "bronze",
        61.1388664,
        21.5142447
      ],
      /* JR .. some kanjis - missing */
      [
        "Jäkäläkivi",
        "silver",
        60.409564,
        22.271062
      ],
      [
        "Järvelän kosteikon esteetön lintulava",
        "silver",
        60.462842,
        22.379424
      ],
      [
        "Jättituija ja pähkinäpensaita",
        "gold",
        60.402704,
        22.391119
      ],
      [
        "Jäähalli Aina",
        "silver",
        60.412742,
        22.358431
      ],
      [
        "Kaarina Info",
        "silver",
        60.415959,
        22.327689
      ],
      [
        "Kaarina- Teatteri",
        "gold",
        60.410263,
        22.373478
      ],
      [
        "Kaarina Water Tower",
        "gold",
        60.404695,
        22.377855
      ],
      [
        "Kaarinan kirkko",
        "bronze",
        60.405376,
        22.365595
      ],
      [
        "Kaarinan posti",
        "bronze",
        60.407852,
        22.359863
      ],
      [
        "Kaarinan Soutukeskus",
        "silver",
        60.404554,
        22.419401
      ],
      [
        "Kaarinatalo",
        "silver",
        60.408453,
        22.367451
      ],
      [
        "Kaarningon vesijohtolaitos",
        "silver",
        60.41967,
        22.31938
      ],
      [
        /* Removed gym */
        "Kahva Sillassa",
        "bronze",
        60.43522,
        22.32482
      ],
      [
        "Kairispaasin leikkipaikka",
        "silver",
        60.410799,
        22.314503
      ],
      [
        "Kalat Sähkökaapissa",
        "silver",
        60.439567,
        22.336735
      ],
      [
        "Kallioristi",
        "silver",
        60.482797,
        22.180034
      ],
      [
        "Kansanpuiston Keltainen Mökki",
        "bronze",
        60.426955,
        22.185606
      ],
      [
        "Kappelimäenpuiston kävelysilta",
        "silver",
        60.41395,
        22.38568
      ],
      [
        "Kappelinmäki",
        "silver",
        60.405801,
        22.465446
      ],
      [
        "Karhulinna",
        "bronze",
        61.1483286,
        21.4719413
      ],
      [
        "Karjala Gun Ship",
        "bronze",
        60.43578,
        22.235278
      ],
      [
        "Karvetin koirapuisto",
        "bronze",
        60.482149,
        22.057204
      ],
      [
        "Karvetin Monitoimitalo",
        "basic",
        60.48064,
        22.048021
      ],
      [
        "Kastunpuiston leikkipaikka",
        "basic",
        60.467902,
        22.276513
      ],
      [
        "Kasvutekijä",
        "bronze",
        60.435589,
        22.172292
      ],
      [
        "Katariinan vuokraviljelypalstat",
        "silver",
        60.412346,
        22.289175
      ],
      [
        "Katariinankivi",
        "silver",
        60.410442,
        22.27439
      ],
      [
        "Katariinanlaakso",
        "silver",
        60.409413,
        22.274338
      ],
      [
        "Kauppa Teollisuus ja Säästäväisyys",
        "bronze",
        60.448607,
        22.2655
      ],
      [
        "Kaupungin Viljelypalsta Halinen",
        "bronze",
        60.466489,
        22.319743
      ],
      [
        "Keisarinlähde",
        "basic",
        60.471054,
        22.013939
      ],
      [
        "Keke",
        "bronze",
        60.4455908,
        22.2455652
      ],
      [
        "Kekomuurahaiset",
        "bronze",
        60.438249,
        22.197319
      ],
      [
        "Kellariteatteri",
        "bronze",
        61.135686,
        21.4865222
      ],
      [
        /* Tampere, Koskikeskus */
        "Keskuskupoli",
        "bronze",
        61.4952362,
        23.7671977
      ],
      [
        "Kesämäen Kenttä",
        "silver",
        60.41502,
        22.39736
      ],
      [
        "Kiikartorni",
        "silver",
        61.1396759,
        21.4712688
      ],
      [
        "Kilosport Areena Kaarina",
        "gold",
        60.415735,
        22.343905
      ],
      [
        "Kingdom Hall of Jehovah's Witnesses",
        "bronze",
        60.455241,
        22.243442
      ],
      [
        /* Might be wrong - no direct hit */
        "Kippax Lake Fountain and Sculpture",
        "bronze",
        -33.889267,
        151.222558
      ],
      [
        "Kirche Döttingen",
        "bronze",
        47.5688675,
        8.2574551
      ],
      [
        "Kirkastuminen",
        "gold",
        60.429216,
        22.307147
      ],
      [
        "Kissa-Kallu",
        "bronze",
        60.455441,
        22.29874
      ],
      [
        "Kivipii",
        "gold",
        60.408888,
        22.382367
      ],
      [
        "Klapperstein",
        "bronze",
        47.746659,
        7.339160
      ],
      [
        "Kohmon ulkoilureitti",
        "bronze",
        60.457249,
        22.344696
      ],
      [
        "Kohmon ulkokuntosali",
        "silver",
        60.455182,
        22.342219
      ],
      [
        "Koirapatsas",
        "basic",
        61.142957,
        21.527333
      ],
      [
        "Kokeiluverstas",
        "silver",
        60.461268,
        22.334603
      ],
      [
        "Kokispullo",
        "basic",
        60.4860741,
        22.143949
      ],
      [
        "Komosten Kummut",
        "silver",
        60.464248,
        22.320919
      ],
      [
        "Kontionpuiston ulkokuntosali",
        "gold",
        60.425506,
        22.336418
      ],
      [
        "Konttikadun Pallokenttä",
        "silver",
        60.40375,
        22.35529
      ],
      [
        "Koppi ja kova luu",
        "bronze",
        60.46462,
        22.287903
      ],
      [
        "Koriston kartanon vilja-aitat",
        "silver",
        60.400201,
        22.356996
      ],
      [
        "Koriston Viljelypalstat",
        "bronze",
        60.40126,
        22.35375
      ],
      [
        "Kosminen lintu",
        "basic",
        60.485725,
        22.171118
      ],
      [
        "Kousankujan leikkipaikka",
        "basic",
        60.44394,
        22.35580
      ],
      [
        "Kuhankuono",
        "basic",
        60.737306,
        22.415063
      ],
      [
        "Kuivassa Kangasmetsässä On Niukasti Eläimiä",
        "silver",
        60.399716,
        22.292738
      ],
      [
        "Kun ystävyyssuhteet solmitaan",
        "silver",
        60.455261,
        22.26988
      ],
      [
        "Kuntoradan opaskyltti - Suokatu",
        "bronze",
        60.44855,
        22.34384
      ],
      [
        "Kuntoradan opaste - Jaani",
        "silver",
        60.454115,
        22.337959
      ],
      [
        "Kuntoratojen opaskyltti - Satakunnantie",
        "bronze",
        60.475669,
        22.20311
      ],
      [
        "Kuovinkadun Leikkipaikka",
        "bronze",
        60.40923,
        22.31914
      ],
      [
        "Kuovinkadun Toimintakeskus",
        "bronze",
        60.409244,
        22.315855
      ],
      [
        "Kupittaa Pesäpallo Stadium",
        "gold",
        60.44679,
        22.29669
      ],
      [
        "Kupittaan Kaivo",
        "silver",
        60.445814,
        22.291882
      ],
      [
        "Kurala Info",
        "basic",
        60.462088,
        22.333685
      ],
      [
        "Kurjenmäen pallokenttä",
        "bronze",
        60.438018,
        22.288698
      ],
      [
        "Kurjenrahka Luontotupa",
        "bronze",
        60.740357,
        22.413981
      ],
      [
        "Kurjenrahkan portti",
        "bronze",
        60.742574,
        22.424710
      ],
      [
        "Kuukkelinpuisto",
        "silver",
        60.43904,
        22.33708
      ],
      [
        "Kuusiston Piispanlinna",
        "basic",
        60.407618,
        22.474242
      ],
      [
        "Kyltit Tuorlan Kivessä",
        "bronze",
        60.416495,
        22.444448
      ],
      [
        "Kärsämäen Urheilupuisto",
        "basic",
        60.481391,
        22.280644
      ],
      [
        "Köydenpunojankadun bunkkeri",
        "bronze",
        60.458701,
        22.258688
      ],
      [
        "La Cori",
        "bronze",
        41.3752374,
        2.0760438
      ],
      [
        "La Fresque",
        "bronze",
        48.839121,
        2.244774
      ],
      [
        "Laitila Info",
        "bronze",
        60.895272,
        21.653758
      ],
      [
        "Lakka",
        "bronze",
        60.745635,
        22.393568
      ],
      [
        "Lampaat laitumella",
        "silver",
        60.400306,
        22.28576
      ],
      [
        "Lapset",
        "bronze",
        60.450382,
        22.260665
      ],
      [
        "Lassilan karhu",
        "basic",
        60.45794,
        22.22557
      ],
      [
        "Latumajan reitti",
        "silver",
        61.142754,
        21.532492
      ],
      [
        "Lauri Viljasenpuiston leikkipaikka",
        "bronze",
        60.456458,
        22.31002
      ],
      [
        "Lauste Beach Volley",
        "silver",
        60.435439,
        22.332504
      ],
      [
        "Lausteen Frisbeegolfpuisto",
        "gold",
        60.428663,
        22.346646
      ],
      [
        /* Poistettu gym */
        "Lausteen Kuntoradan Gym",
        "silver",
        60.42900,
        22.34636
      ],
      [
        /* Vanha nimi: Maistraatinpolun Pallokenttä */
        "Lausteen pallokenttä",
        "gold",
        60.43179,
        22.34751
      ],
      [
        "Lausteenpuiston leikkipaikka",
        "gold",
        60.430661,
        22.343183
      ],
      [
        "Lehtorinne Ja Kevätkasvit",
        "silver",
        60.410318,
        22.273042
      ],
      [
        "Leivosenkadun leikkipaikka (removed)",
        "bronze",
        60.441066,
        22.336246
      ],
      [
        "Lemun kaatuneiden muistomerkki",
        "silver",
        60.39212,
        22.316814
      ],
      [
        "Leningrad Partnership Tree",
        "silver",
        60.445272,
        22.289491
      ],
      [
        "Lenkkipolkujen Opasteet",
        "silver",
        60.435747,
        22.317216
      ],
      [
        "Lenkkipolun Opasteet",
        "bronze",
        60.413782,
        22.297686
      ],
      [
        "Light Installation",
        "basic",
        60.461634,
        22.285559
      ],
      [
        "Linkkitorni",
        "gold",
        60.446876,
        22.332926
      ],
      [
        "Lintuja",
        "silver",
        60.461332,
        22.393696
      ],
      [
        "lintutorni suossa",
        "silver",
        60.407818,
        22.286668
      ],
      [
        "Littoisten Lintutorni",
        "bronze",
        60.46138,
        22.384394
      ],
      /* Miksi näitä pitää olla kaksi eriä!? */
      [
        "Littoisten luontopolku",
        "silver",
        60.452009,
        22.402167
      ],
      [
        "Littoisten Luontopolku",
        "silver",
        60.460767,
        22.386689
      ],
      [
        "Littoisten Posti",
        "silver",
        60.433039,
        22.385299
      ],
      [
        "Littoisten Seurakuntatalo",
        "bronze",
        60.445871,
        22.385191
      ],
      [
        "Littoisten Verkatehdas",
        "bronze",
        60.45052,
        22.403703
      ],
      [
        "Littoisten Verkatehtaan puistot",
        "bronze",
        60.449873,
        22.404825
      ],
      [
        "Littoistentien ylikulkusilta",
        "silver",
        60.44382,
        22.33303
      ],
      [
        "Liukumäki",
        "silver",
        60.432936,
        22.355719
      ],
      [
        /* Tampere */
        "Locks of Love",
        "basic",
        61.500253,
        23.762299
      ],
      [
        /* Hervanta */
        "Log Gazebo",
        "bronze",
        61.4471606,
        23.8342098
      ],
      [
        "Lonely Robot",
        "bronze",
        60.449186,
        22.301129
      ],
      [
        /*  3 Spiggots Cl, Longstanton, Cambridge CB24 3EA, UK */
        "Longstanton Recreation Gound Playground",
        "bronze",
        52.281932,
        0.042864
      ],
      [
        "Love Birds",
        "basic",
        60.439242,
        22.258547
      ],
      [
        "Lubbock Baptist Temple",
        "bronze",
        33.5394215,
        -101.8872172
      ],
      [
        "Lumme ja Ulpukka Infokyltti",
        "silver",
        60.428011,
        22.281961
      ],
      [
        "Luolavuoren Labyrintti",
        "bronze",
        60.428011,
        22.281961
      ],
      [
        "Luolavuoren pallokenttä",
        "basic",
        60.434407,
        22.277352
      ],
      [
        "Luolavuoren Vesitorni",
        "basic",
        60.429015,
        22.279072
      ],
      [
        /* Savojärvellä */
        "Luontopolku",
        "silver",
        60.743469,
        22.390009
      ],
      [
        "Lähdepellon Kuntoradat",
        "silver",
        61.107926,
        21.527709
      ],
      [
        "Maaltamuutto",
        "bronze",
        61.503964,
        23.758519
      ],
      [
        "Maarian pappila",
        "bronze",
        60.474709,
        22.293277
      ],
      [
        /* Rauma */
        "Maauimala",
        "silver",
        61.1377174,
        21.4726164
      ],
      [
        "Majanummen Toimitila",
        "silver",
        60.441428,
        22.360379
      ],
      [
        "Mallaskadun leikkikenttä",
        "basic",
        60.41139,
        22.32644
      ],
      [
        "Manhattan Sport Center",
        "basic",
        60.457409,
        22.234578
      ],
      [
        "Mansikkakoto",
        "basic",
        60.442645,
        22.2683
      ],
      [
        "Mansinpuisto Leikkipaikka",
        "silver",
        60.43529,
        22.35926
      ],
      [
        "Marjamaanpuiston Leikkipaikka",
        "silver",
        60.438821,
        22.309488
      ],
      [
        "Marli Areena",
        "silver",
        60.442459,
        22.288616
      ],
      [
        "Matkan varrelta",
        "basic",
        60.43740,
        22.25902
      ],
      [
        /* 160 Meadowlands Boulevard, Ancaster, ON L9K, Canada */
        "Meadowlands Park",
        "bronze",
        3.220371,
        -79.9466
      ],
      [
        "Merikotkat",
        "silver",
        60.454366,
        22.2892
      ],
      [
        "Merimieskirkko Kyltti",
        "basic",
        60.449796,
        22.200191
      ],
      [
        "Merionnettomuuden Uhrien Muistoksi",
        "silver",
        61.136899,
        21.368008
      ],
      [
        "Merirauma Info",
        "silver",
        61.149009,
        21.486070
      ],
      [
        "Meriristi Public Beach",
        "bronze",
        61.149570,
        21.463728
      ],
      [
        "Metsämäki Entrance",
        "basic",
        60.495,
        22.34667
      ],
      [
        "Metsäpub Lauste",
        "silver",
        60.435154,
        22.346793
      ],
      [
        "Mikaelinkirkko",
        "bronze",
        60.448822,
        22.24851
      ],
      [
        "Mikkolanmäen IT-bunkkeri 1",
        "silver",
        60.443229,
        22.318044
      ],
      [
        "Mikkolanmäen IT-bunkkeri 4",
        "gold",
        60.442884,
        22.31835
      ],
      [
        "Miso",
        "silver",
        60.406595,
        22.342506
      ],
      [
        "Molskipuisto",
        "bronze",
        60.444609,
        22.402001
      ],
      [
        "Muikunvuoren rautakautinen kalmisto",
        "bronze",
        60.46763,
        22.35560
      ],
      [
        /* Näsijärvenkatu 5, 33210 Tampere, Finland */
        "Mummo ja lapsi",
        "bronze",
        61.502906,
        23.750541
      ],
      [
        "Munsterinkadun Leikkipaikka&Huvimaja",
        "bronze",
        60.488717,
        22.255578
      ],
      [
        "Mushroom Graffiti",
        "silver",
        60.418699,
        22.302799
      ],
      [
        "Muumitalo",
        "basic",
        60.472871,
        22.003723
      ],
      [
        "Naantalin Kirjasto",
        "basic",
        60.467187,
        22.024472
      ],
      [
        "Nasulammen Nuotiopaikka",
        "bronze",
        61.14601,
        21.540318
      ],
      [
        "Nasulammen Tarkkailulava",
        "silver",
        61.145728,
        21.541884
      ],

      /* Not found on ingress.. name changed / removed or just no on it? Austria
      [
        "Nimm dir Zeit und lass dich nieder",
        "bronze",
       
      ],
      */
      [
        "Nokan Taidetorppa",
        "silver",
        61.1395253,
        21.4707883
      ],
      [
        "Nuotiopaikka Kylmäpihlajassa",
        "bronze",
        61.143277,
        21.304669
      ],
      [
        "Nyplääjäpatsas",
        "basic",
        61.126419,
        21.509938
      ],
      [
        "Ohjattu kasvu",
        "basic",
        60.485765,
        22.139646
      ],
      [
        "Ohjeet",
        "silver",
        60.39864,
        22.374658
      ],
      [
        "Olavi Virta Memorial",
        "bronze",
        61.4989392,
        23.7717366
      ],
      [
        "Old Aircompressor",
        "silver",
        61.104265,
        21.506797
      ],
      [
        "Old Church Ruins",
        "basic",
        61.1269331,
        21.5156535
      ],
      [
        "Old Factory Chimney",
        "basic",
        60.459698,
        22.280035
      ],
      [
        "Old Family Shrine",
        "bronze",
        34.759024,
        135.506073
      ],
      [
        "Old Mill",
        "bronze",
        60.452369,
        22.299752
      ],
      [
        "Old Navy Artillery Cannon",
        "silver",
        61.133972,
        21.366619
      ],
      [
        "Oman orren alla",
        "basic",
        60.483492,
        22.05303
      ],
      [
        "Omena ja Härkiä",
        "bronze",
        60.45558,
        22.30189
      ],
      [
        "Onnenhevonen",
        "bronze",
        60.450883,
        22.262398
      ],
      [
        "Orikedon Posti",
        "basic",
        60.484452,
        22.313347
      ],
      [
        /*  2 Chome-7-12 Izumi, Suginami-ku, Tōkyō-to, Japan */
        "O-Zizo-Sama",
        "bronze",
        35.670927,
        139.651204
      ],
      [
        "Paalupaikka",
        "basic",
        60.466333,
        22.247352
      ],
      [
        "Paattisten kirkko",
        "basic",
        60.591278,
        22.380934
      ],
      [
        /* Keskustan patsas */
        "Paavo Nurmi",
        "silver",
        60.447975,
        22.270034
      ],
      [
        /* Hauta */
        "Paavo Nurmi",
        "silver",
        60.431676,
        22.309618
      ],
      [
        "Padelkenttä",
        "bronze",
        60.446972,
        22.319426
      ],
      [
        "PadelPark Skanssi",
        "bronze",
        60.436582,
        22.333013
      ],
      [
        "Panimon valas",
        "bronze",
        60.414536,
        22.322811
      ],
      [
        "Papa Frita",
        "bronze",
        41.370360,
        2.090053
      ],
      [
        "Papinholman nuortentalo",
        "bronze",
        60.39701,
        22.38867
      ],
      [
        "Paratiisipilkkulintu",
        "silver",
        60.439757,
        22.334334
      ],
      [
        "Parc Nérée-Tremblay",
        "bronze",
        46.78947,
        -71.282553
      ],
      [
        "Parvi",
        "bronze",
        60.435511,
        22.171951
      ],
      [
        "Passerelle des Trois Frontières",
        "bronze",
        47.591540,
        7.590321
      ],
      [
        "Peltolan siirtolapuutarha",
        "basic",
        60.422022,
        22.294417
      ],
      [
        "Petreliuksen Uimahalli",
        "bronze",
        60.431221,
        22.293813
      ],
      [
        "Piikkiön Asema",
        "basic",
        60.42324,
        22.51566
      ],
      [
        "Piikkiön Kirjasto",
        "bronze",
        60.425135,
        22.517494
      ],
      [
        "Piikkiön kirkko",
        "basic",
        60.425723,
        22.51877
      ],
      [
        "Piispanlähteen jalkapallokenttä",
        "bronze",
        60.406333,
        22.321445
      ],
      [
        "Pikkunotkon leikkipaikka",
        "bronze",
        60.460708,
        22.350215
      ],
      [
        "Pikkupääskyn leikkipaikka",
        "bronze",
        60.442602,
        22.338726
      ],
      [
        "Pohjoiskehän Asiamiesposti",
        "bronze",
        61.148847,
        21.492196
      ],
      [
        "Poika Ja Lyhty",
        "basic",
        60.469118,
        22.264979
      ],
      [
        "Poikluoman Seurakuntatalo",
        "silver",
        60.41137,
        22.33103
      ],
      [
        "POP Pankki Areena",
        "basic",
        60.420764,
        22.471899
      ],
      [
        /* Zoolandia */
        "Poro",
        "silver",
        60.564743,
        22.434006
      ],
      [
        "Poropuiston pallokenttä",
        "gold",
        60.431061,
        22.334519
      ],
      [
        /* Via Machiavelli, 25, 00185 Rome, Italy */
        "Portale Chiesa Suore Agostiniane",
        "bronze",
        41.892556,
        12.503061
      ],
      [
        "Posankka",
        "silver",
        60.458578,
        22.289602
      ],
      [
        /* Mynämäen taukopaikka */
        "P-Paikan Gazebo",
        "gold",
        60.706903, 
        21.968833
      ],
      [
        "Pro Patria",
        "basic",
        60.461674,
        22.025348
      ],
      [
        "Puhuri",
        "basic",
        60.446814,
        22.353004
      ],
      [
        "Puistosali",
        "basic",
        60.398906,
        22.374091
      ],
      [
        "Puolaajanpuisto",
        "bronze",
        60.458974,
        22.414123
      ],
      [
        "Puron varren laulajia",
        "silver",
        60.40053,
        22.391199
      ],
      [
        "Pyhän Aleksanteri Nevalaisen ja Pyhän Nikolaoksen kirkko",
        "basic",
        61.495222,
        23.771513
      ],
      [
        "Pyhän Katariinan kirkko",
        "basic",
        60.462273,
        22.293917
      ],
      [
        "Pyhän Ristin Kirkko",
        "silver",
        61.129722,
        21.511981
      ],
      [
        "Pyramid of glass",
        "bronze",
        60.447861,
        22.259431
      ],
      [
        "Pyynpään Leikkipuisto",
        "bronze",
        61.138932,
        21.5076382
      ],
      [
        "Pyytjärven Kuntorata",
        "silver",
        61.155658,
        21.495189
      ],
      [
        "Pyytjärven ulkokuntoilupaikka",
        "silver",
        61.156820,
        21.498099
      ],
      [
        /* Pyytjärven infotaulu ja kuntorata voi olla päittäin tms sekaisin */
        "Pyytjärvi infotaulu",
        "basic",
        61.154173,
        21.493991
      ],
      [
        /* Lapin kahvila */
        "Pyörni Bear Statue",
        "silver",
        61.0988708,
        21.8272065
      ],
      [
        "Pääskyvuoren bunkkeri",
        "gold",
        60.446685,
        22.332081
      ],
      [
        "Pökkelöpesijät",
        "silver",
        60.438182,
        22.195748
      ],
      [
        "Raadelman vanha muuntoasema",
        "silver",
        60.41874,
        22.45520
      ],
      [
        "Raastuvanpuiston koirapuisto",
        "silver",
        60.43322,
        22.35218
      ],
      [
        "Rabbits Mural",
        "basic",
        60.446662,
        22.317625
      ],
      [
        "Race Track For Offroad Rc-cars",
        "bronze",
        61.12618,
        21.488294
      ],
      [
        "Rafu ja Teresia Lönnströmin puisto",
        "silver",
        61.141728,
        21.483400
      ],
      [
        "Raision Kaupungintalo",
        "bronze",
        60.485479,
        22.172463
      ],
      [
        "Raision Kirkkomaan Pääportti",
        "basic",
        60.48426,
        22.17547
      ],
      [
        "Raision Kirkko Cross",
        "bronze",
        60.482797,
        22.180034
      ],
      [
        "Raision Seurakuntatalo",
        "silver",
        60.483249,
        22.175179
      ],
      [
        "Rantapuiston kenttä",
        "bronze",
        60.39972,
        22.37417
      ],
      [
        "Ratsastajanpuisto",
        "bronze",
        60.416934,
        22.46067
      ],
      [
        "Rauhalinnan luontopolku",
        "gold",
        60.402257,
        22.3971
      ],
      [
        "Rauma Railway Station",
        "basic",
        61.1306601,
        21.4836947
      ],
      [
        "Rauman Rautatien Muistomerkki",
        "basic",
        61.131847,
        21.492786
      ],
      [
        "Rauman Uimahalli",
        "bronze",
        61.122414,
        21.499112
      ],
      [
        "Raumanmerenpuiston leikkikenttä",
        "bronze",
        61.132935,
        21.482471
      ],
      [
        "Rauma-sali",
        "silver",
        61.127613,
        21.481733
      ],
      [
        "Raunistulan Seurakuntatalo",
        "basic",
        60.461534,
        22.268974
      ],
      [
        "Rauvolanlahden Luonnonsuojelualue",
        "silver",
        60.410321,
        22.285939
      ],
      [
        "Rauvolanlahti",
        "bronze",
        60.404252,
        22.294719
      ],
      /* Tavuviiva sotkee haun.. ei löy'y ingressin haulla
      [
        "Rebhalden-Frau mit Korb",
        "bronze",
        ?
      ],
      */
      [
        "Red Wooden Electricity Tower",
        "silver",
        60.462168,
        22.334973
      ],
      [
        "Rekolantien Leikkipuisto",
        "basic",
        60.424380,
        22.502998
      ],
      [
        "Rekolantien pallokenttä",
        "basic",
        60.426877,
        22.504140
      ],
      [
        "Retkeilyreitin Opasteet",
        "gold",
        60.395687,
        22.304709
      ],
      [
        "Ristikallion uimaranta",
        "silver",
        60.447184,
        22.397436
      ],
      [
        "Rosanpuiston leikkipaikka",
        "basic",
        60.42220,
        22.27087
      ],
      [
        "Ruiskutin",
        "silver",
        60.567293,
        22.435849
      ],
      [
        "Runosmäen Alikulkutunnelin Seinämaalaus - Eläimet",
        "basic",
        60.483134,
        22.273692
      ],
      [
        "Runosmäen Alikulkutunnelin Seinämaalaus - Sienet",
        "basic",
        60.483329,
        22.273085
      ],
      [
        "Ruuvipurkain CSR-2",
        "bronze",
        61.123989,
        21.504530
      ],
      [
        "Saarisen kenttä",
        "silver",
        60.404407,
        22.361542
      ],
      [
        "Sadetyttö ja makeisputto",
        "basic",
        60.486039,
        22.286672
      ],
      [
        "Saharannan uimaranta",
        "silver",
        61.142950,
        21.472038
      ],
      [
        "Satakielenpuiston leikkipaikka",
        "silver",
        60.439231,
        22.33393
      ],
      [
        "Sauhuvuoren Pronssikautinen Hauta",
        "gold",
        60.40224,
        22.31469
      ],
      [
        "Savojärven kierros - opaskyltti",
        "silver",
        60.736782,
        22.415208
      ],
      [
        "Savotta",
        "bronze",
        60.434947,
        22.171837
      ],
      [
        "Scène Du Parc",
        "bronze",
        46.515728,
        6.646297
      ],
      [
        "Sepen grillin leikkipaikka",
        "silver",
        60.419103,
        22.380563
      ],
      [
        "Seurakuntatalon leikkipaikka",
        "bronze",
        60.44566,
        22.38469
      ],
      [
        "Silvolanpuisto",
        "bronze",
        60.463459,
        22.353377
      ],
      [
        "Solmupuisto",
        "bronze",
        60.46595,
        22.36492
      ],
      [
        "Sotiemme Veteraanit 1939-1945",
        "basic",
        60.593862,
        22.383551
      ],
      [
        "Soukkion Kenttä",
        "silver",
        60.41448,
        22.410937
      ],
      [
        "Steam Packet Inn",
        "bronze",
        50.4274284,
        -3.6826403
      ],
      [
        "Stone Pargas",
        "bronze",
        60.297372,
        22.297892
      ],
      [
        "Street Art Mosaïque",
        "bronze",
        47.749573,
        7.32703
      ],
      [
        "Sulhontien Leikkikenttä",
        "bronze",
        60.576117,
        22.098629
      ],
      [
        "Suomen Sydän 7",
        "silver",
        60.465387,
        22.307181
      ],
      [
        "Suomen Sydän 8",
        "bronze",
        60.465258,
        22.325741
      ],
      [
        /* Tampereella */
        "Suomi-neito",
        "basic",
        61.497803,
        23.763246
      ],
      [
        "Suonsilmä",
        "silver",
        60.74528,
        22.390309
      ],
      [
        "Taidetta Tolpassa",
        "silver",
        60.451851,
        22.403123
      ],
      [
        /* Kurjenrahka */
        "Talousmetsä",
        "gold",
        60.745231,
        22.38709
      ],
      [
        "Talvisodan vauriot 29.1.1940",
        "bronze",
        60.449146,
        22.261979
      ],
      [
        "Tammi - Maakuntamme Symboli",
        "bronze",
        60.412702,
        22.272603
      ],
      [
        "Tampere bus station",
        "bronze",
        61.49364,
        23.769034
      ],
      [
        "Tampere-talo",
        "bronze",
        61.496055,
        23.7803
      ],
      [
        "Tasala Vilkk ja Hakkri Iiro",
        "bronze",
        61.129986,
        21.501516
      ],
      [
        "Tayside Hotel",
        "bronze",
        56.483145,
        -3.448187
      ],
      [
        "Teatteri Emma",
        "basic",
        60.473385,
        22.006496
      ],
      [
        "Teen Sinut Taas Ehjäksi",
        "basic",
        60.404199,
        22.465795
      ],
      [
        "Telkänkatu playground",
        "bronze",
        61.1387012,
        21.5038748
      ],
      [
        "Tervetuloa Kuralan Kylämäkeen",
        "silver",
        60.45976,
        22.33591
      ],
      [
        "The Cochlear Centre",
        "bronze",
        -33.777215,
        151.113417
      ],
      [
        "The Old Border Stone",
        "silver",
        60.427155,
        22.312784
      ],
      [
        "The Rainbow's Treasure - solukalvo",
        "gold",
        60.43063,
        22.33063
      ],
      [
        "The Royal Seven Stars Hotel Sign",
        "bronze",
        50.4309404,
        -3.6829641
      ],
      [
        "The Why",
        "basic",
        60.465627,
        22.274784
      ],
      [
        "Timantteja, tähdenlentoja, perinteitä",
        "bronze",
        61.120562,
        21.504818
      ],
      [
        /* Toboggan (42-62 Résidence du Parc des Eaux Vives, 91120 Palaiseau, France) */
        "Toboggan",
        "bronze",
        48.703134,
        2.238379
      ],
      [
        "Tornihotelli",
        "basic",
        61.4968689,
        23.7731174
      ],
      [
        "Trivium",
        "bronze",
        60.445961,
        22.300579
      ],
      [
        /* 5 Chome-23-13 Chiyoda, Naka Ward, Nagoya, Aichi, Japan */
        "Tsurumai Park 歴史公園百選",
        "bronze",
        35.156605,
        136.918165
      ],
      [
        "Tummel Stone",
        "bronze",
        60.304061,
        22.323191
      ],
      [
        "Tuomaansilta",
        "bronze",
        60.459089,
        22.278852
      ],
      [
        "Tuomiokirkkosilta",
        "silver",
        60.452256,
        22.274608
      ],
      [
        "Tuomoniemen Matonpesupaikka",
        "bronze",
        61.152922,
        21.459705
      ],
      [
        "Tuorla Observatorio",
        "silver",
        60.416216,
        22.446138
      ],
      [
        "Tuorlan Huvimaja",
        "silver",
        60.415974,
        22.437331
      ],
      [
        "Turku Central Railway Station",
        "bronze",
        60.45385,
        22.253157
      ],
      [
        /* Tampereen valtatie 122, 20360 Turku, Finland */
        "Turku Info",
        "basic",
        60.487557,
        22.276918
      ],
      [
        "Turun - Uudenkaupungin Rata 50 Vuotta 1.9.1974",
        "basic",
        60.453684,
        22.252664
      ],
      [
        "Turun Ammattikorkeakoulu",
        "silver",
        60.447899,
        22.299812
      ],
      [
        "Turun linna",
        "basic",
        60.43521,
        22.22753
      ],
      [
        "Turun Paras Lintulahti",
        "silver",
        60.41086,
        22.277182
      ],
      [
        "Turun Rauhanyhdistys Ry",
        "bronze",
        60.444657,
        22.368937
      ],
      [
        "Turun Terveydenhuoltomuseo",
        "bronze",
        60.44139,
        22.27518
      ],
      [
        "Turun tuomiokirkko",
        "silver",
        60.452426,
        22.277673
      ],
      [
        "Turun Vesilaitos",
        "basic",
        60.462594,
        22.304693
      ],
      [
        "Turun yliopisto - Educarium",
        "gold",
        60.458338,
        22.285539
      ],
      [
        "Tweet Tweet",
        "silver",
        60.4297,
        22.323529
      ],
      [
        "Tähkäpuisto",
        "bronze",
        60.432931,
        22.296511
      ],
      [
        "Tähkäpään Tila",
        "bronze",
        60.469277,
        22.367139
      ],
      [
        "Tähtiin Tähyävät: Huomenna",
        "bronze",
        60.445662,
        22.257749
      ],
      [
        "Tähtimökatu 5:n leikkipaikka",
        "basic",
        60.45183,
        22.330356
      ],
      [
        "Uhrikuoppia Kalliossa",
        "silver",
        60.464346,
        22.315365
      ],
      [
        "Uittamon Posti",
        "basic",
        60.42161,
        22.26408
      ],
      [
        "Uittamon seurakuntakoti",
        "basic",
        60.41810,
        22.26270
      ],
      [
        "Ulkoliikuntapaikka",
        "gold",
        60.426361,
        22.354863
      ],
      [
        "Untamalan Kirkko",
        "basic",
        60.9082611,
        21.6332582
      ],
      [
        "Untamalan Kylänraitti",
        "basic",
        60.908308,
        21.6335586
      ],
      [
        "Untolantien leikkipaikka",
        "silver",
        60.43696,
        22.38151
      ],
      [
        "Urheilutalo Ooperi",
        "bronze",
        61.1008072,
        21.5074416
      ],
      [
        "Uskon Portti",
        "silver",
        60.428084,
        22.518205
      ],
      [
        "Uudenlahden Kota",
        "bronze",
        61.153085,
        21.496307
      ],
      [
        "V.I.Lenin Muistopatsas",
        "silver",
        60.44433,
        22.39439
      ],
      [
        "Vaahtera",
        "silver",
        60.416131,
        22.449821
      ],
      [
        "Vaarniemenkallio Lookout",
        "silver",
        60.39921,
        22.288206
      ],
      [
        "Vahva",
        "basic",
        60.445235,
        22.226157
      ],
      [
        "Valkoinen Talo 1927",
        "basic",
        60.528948,
        22.425665
      ],
      [
        "Valkomaksaruoho",
        "silver",
        60.431484,
        22.296155
      ],
      [
        /* Rauma, Otanlahti */
        "Wall Art",
        "basic",
        61.134571,
        21.484210
      ],
      [
        /* Rauman sairaalan vieressä */
        "Vanha Kaivo",
        "bronze",
        60.446448,
        22.403271
      ],
      [
        /* Littoisten kentän vieressä */
        "Vanha Kaivo",
        "bronze",
        60.44645,
        22.40327
      ],
      [
        /* Kuuskajaskari */
        "Wanha Kasarmi",
        "silver",
        61.136735,
        21.367486
      ],
      [
        "Vapaakirkko",
        "bronze",
        60.455767,
        22.267757
      ],
      [
        /* Tampere, hämeenpuistossa */
        "Vapaudenpatsas",
        "basic",
        61.4974424,
        23.7498924
      ],
      [
        /* Rauma, keskuspuiston vieressä */
        "Vapaudenpatsas",
        "bronze",
        61.130618,
        21.501700
      ],
      [
        "Varissuo Church",
        "bronze",
        60.443986,
        22.358491
      ],
      [
        "Varissuon kuntoradan opaste",
        "silver",
        60.45436,
        22.34106
      ],
      [
        "Varissuon Lämpö oy Chimneys",
        "bronze",
        60.44404,
        22.36452
      ],
      [
        "Varppeenkadun gazebo",
        "basic",
        60.48558,
        22.15385
      ],
      [
        "Varsinais-Suomen Puisto",
        "bronze",
        60.464398,
        22.293524
      ],
      [
        "Vasaramäen Liikuntaopasteet",
        "silver",
        60.44188,
        22.31016
      ],
      [
        "Watch Platform 3",
        "silver",
        60.405838,
        22.285042
      ],
      [
        /* Rauma: Kuuskajaskarin tulenjohtotorni */
        "Watch tower",
        "basic",
        61.137674,
        21.364964
      ],
      [
        /* Address: 1475-263 Kizawa, Oyama-shi, Tochigi-ken 323-0014, Japan */
        "Water Plaza Stage",
        "bronze",
        36.33582,
        139.807858
      ],
      [
        "Water Tower Fort",
        "silver",
        60.461846,
        22.336478
      ],
      [
        /* Address: Hankkarintie 11, 26100 Rauma, Finland */
        "Watermill",
        "basic",
        61.113938,
        21.500653
      ],
      [
        "Vatsela",
        "basic",
        60.489466,
        22.124295
      ],
      [
        "Vedenpuhdistaminen",
        "silver",
        60.403949,
        22.292877
      ],
      [
        "Venäläinen tykkipatteri No: 2",
        "basic",
        60.422754,
        22.169013
      ],
      [
        "Veritas Stadion",
        "silver",
        60.443078,
        22.290429
      ],
      [
        "Veteraanipuiston Rosarium",
        "gold",
        60.407638,
        22.377043
      ],
      [
        /* Raisio */
        "Veteran Commemorative Stone",
        "basic",
        60.484852,
        22.173277
      ],
      [
        "VG Naantali",
        "basic",
        60.466483,
        22.025442
      ],
      [
        "Viheltelijöitä ja huhuilijoita",
        "gold",
        60.401916,
        22.389879
      ],
      [
        "Viitapihlaja-angervo",
        "bronze",
        60.431623,
        22.297417
      ],
      [
        "Vilhonkatu",
        "bronze",
        60.441505,
        22.256236
      ],
      [
        "Villa Promenade Cafe",
        "basic",
        60.427657,
        22.181199
      ],
      [
        "Virnamäen muinaisjäännösalue",
        "basic",
        60.464996,
        22.313932
      ],
      [
        "Visakoivu - Kotoinen Arvopuumme",
        "silver",
        60.411835,
        22.274925
      ],
      [
        "Voivalan venesatama",
        "bronze",
        60.404464,
        22.418001
      ],
      [
        "Wooden Bear",
        "basic",
        60.985551,
        21.586940
      ],
      [
        /* Ihoden Asema */
        "Wooden horse",
        "bronze",
        60.976781,
        21.591812
      ],
      [
        "Vuoden 1918 sankarimonumentti",
        "silver",
        60.452225,
        22.279203
      ],
      [
        "Vuorenneito",
        "basic",
        60.43042,
        22.28763
      ],
      [
        /* Vähäkinnontie 5, 26100 Rauma, Finland */
        "Vähäkinnontien leikkikenttä",
        "silver",
        61.138796,
        21.512296
      ],
      [
        "Välkommen Mariehamn!",
        "basic",
        60.090858,
        19.929968
      ],
      [
        "Välskärinkujan ulkokuntoilusali",
        "gold",
        60.418815,
        22.314092
      ],
      [
        "Vättilän leikkipuisto",
        "silver",
        60.41418,
        22.37090
      ],
      [
        /* Rauman hesen lähellä */
        "Yhteislyseo Plate",
        "silver",
        61.130171,
        21.505349
      ],
      [
        "Yli-Maarian Seurakuntatalo",
        "basic",
        60.535017,
        22.306694
      ],
      [
        "Ylösnousemuskappeli",
        "silver",
        60.433586,
        22.310192
      ],
      [
        /* Miten? Victories: 38 ja Treats 131 > muuallakin tälläinen patsas? 
          ..ja nyt victories: 9 ja treats 37 -> miten nää laskee alaskpäin?
          bugit ohi, nyt on nollassa :O (2022-12) */
        "Youth on Horseback",
        "bronze",
        51.5194133,
        -0.1291453
      ],
      [
        "Yrittäjäpuisto",
        "silver",
        61.130652,
        21.505657
      ],
      [
        "Yrjänänpuiston leikkipaikka",
        "basic",
        60.46004,
        22.26320
      ],
      [
        "Ystävyyden metsä",
        "bronze",
        60.445812,
        22.237822
      ],
      [
        /* 2 Chome-1-34 Shinmachi, Utsunomiya-shi, Tochigi-ken 320-0831, Japan */
        "Zelkova of Shinmachi（新町のケヤキ）",
        "bronze",
        36.548444,
        139.875403
      ],
      [
        "Zoolandia",
        "bronze",
        60.570931,
        22.4382
      ],
      [
        "Åbo Teaterhus",
        "silver",
        60.450928,
        22.266103
      ],
      [
        "Äkkiä loiskahtaa",
        "basic",
        60.744048,
        22.390435
      ],
      [
        /* Miyahara Kyujo Dori, １丁目-１ Yonan, Utsunomiya, Tochigi Prefecture 320-0835, Japan */
        "アトリエ通り 10th NO.22",
        "bronze",
        36.54,
        139.875
      ],
      [
        /* Miyahara Kyujo Dori, １丁目-１ Yonan, Utsunomiya, Tochigi Prefecture 320-0835, Japan */
        "アトリエ通り 10th No.26",
        "bronze",
        36.540333,
        139.877
      ],
      [
        /* 1-1 Hiyoshicho, Fuchu, Tokyo, Japan */
        "アンパンマンと仲間たち",
        "bronze",
        35.665182,
        139.488574
      ],
      /* tähän jäi 3.12.2022 */
      [
        /* Näitä olisi monta.. Icho Park. 50-564 Inuzuka, Oyama-shi, Tochigi-ken 323-0811, Japan */
        "いちょう公園",
        "bronze",
        36.3246,
        139.82748
      ],
      [
        /* 1 Chome-9 Sakamotochō, Yokosuka-shi, Kanagawa-ken, Japan */
        "うらが道枝道の碑",
        "bronze",
        35.274752,
        139.657781
      ],
      [
        /* Ei löydy. Näitä Jizo-patsaita on liikaa: https://www.okujapan.com/blog/japanese-jizo-statues */
        "お地蔵さん",
        "bronze",
        0,
        0
      ],
      [
        /* Ei löydy. Jizo eri merkeillä.. */
        "お地蔵様",
        "bronze",
        0,
        0
      ],
      [
          /* Google löysi: 2 Chome-25-4 Shoji, Ikuno Ward, Osaka, 544-0002, Japani */
        "こすもす カフェ",
        "bronze",
        34.661591,
        135.5527759
      ],
      [
        /* 130-3 Minakuchichō Kitanaiki, Koka, Shiga 528-0051, Japan */
        "こどもの森東側キャンプ場",
        "bronze",
        34.958403,
        136.163834
      ],
      [
        /* 10 Chome-1-6 Kita 1 Jōnishi, Chūō-ku, Sapporo-shi, Hokkaidō, Japan */
        "スープカレー屋のフクロウ",
        "bronze",
        43.061096,
        141.341399
      ],
      [
        /* 1 Chome-3-4 Hōnan, Suginami-ku, Tōkyō-to, Japan */
        "ステゴサウルスの背中",
        "bronze",
        35.674361,
        139.660237
      ],
      [
        /* Google maps: Tully's Coffee, 1 Chome-12-17 Umeda, Kita Ward, Osaka, 530-0001, Japani */
        "タリーズコーヒ梅田スク",
        "bronze",
        34.700473,
        135.4957793
      ],
      [
        /* Japan, 〒519-3204 Mie-ken, Kitamuro-gun, Kihoku-chō, Higashinagashima, 県道751号線 */
        "マンボウ",
        "bronze",
        34.211532,
        136.334574
      ],
      [
        /* 1-33 Toyotsucho, Suita, Osaka Prefecture, Japan */
        "リオちゃん",
        "bronze",
        34.76052,
        135.496113
      ],
      [
        /* Ei tarkka. Mapsilla: Sangdong Lake Park Hill Playground, */
        "상동호수공원언덕놀이터",
        "bronze",
        37.501546,
        126.745041
      ],
      [
        /* Descistä arvattu. Tarkoittaa leikkikenttää. Desc:
            인천광역시 부평구 부개3동에 있는 어린이 놀이터입니다.
            It is a children's playground in Bugae 3-dong, Bupyeong-gu, Incheon.
        */
        "어린이놀이터",
        "bronze",
        37.5031163,
        126.7279184
      ],
      [
        /* Ei ingressissä? crown playground */
        "왕관놀이터",
        "bronze",
        0,
        0
      ],
      [
        /* 3-chōme-25-44 Mitsumatamachi, Maebashi, Gunma 371-0018, Japan */
        "三俣町五反田公園",
        "bronze",
        36.400605,
        139.084852
      ],
      [
        /* 2 Chome-21-5 Kamikoidemachi, Maebashi-shi, Gunma-ken 371-0037, Japan */
        "上小出国体公園",
        "bronze",
        36.414917,
        139.057198
      ],
      [
        /* Japan, 〒355-0015 Saitama-ken, Higashimatsuyama-shi, Honchō 県道66号線 */
        "上沼公園",
        "bronze",
        36.044608,
        139.405197
      ],
      [
        /* 1071 Odani, Akaiwa-shi, Okayama-ken 709-0806, Japan */
        "中八幡宮",
        "bronze",
        34.76853,
        134.023601
      ],
      [
        /* 2701-5 Takaonomachi Shibahiki, Izumi-shi, Kagoshima-ken 899-0402, Japan */
        "中部地区農村運動公園",
        "bronze",
        32.056655,
        130.30233
      ],
      [
        /* Nakanogaisaku Public Hall: Japan, 〒759-0203 Yamaguchi-ken, Ube-shi, Nakanogaisaku, 中野開作公会堂 */
        "中野開作公会堂",
        "bronze",
        33.987589,
        131.218543
      ],
      /*[
        "",
        "bronze",
        32.056655,
        130.30233
      ],*/
      
      

      /* Special characters */
      
      
      [
        "焼津郵便局 YAIZU-TOYODA POST OFFICE",
        "bronze",
        34.8625124,
        138.3027581
      ],
      [
        /* Sanuki Daini Jido Park */
        "佐貫第二児童公園",
        "bronze",
        35.9341441,
        140.1395807
      ],
      [
        /* Miyazaki Post Office */
        "宮崎郵便局",
        "bronze",
        38.6131833,
        140.7557086
      ],
      [
        /* Kagoshima Prefectural Museum */
        "鹿児島県立博物館",
        "bronze",
        31.5939468,
        130.5504708
      ],
      [
        /* Tsunoda Park */
        "角田公園",
        "bronze",
        37.9695148,
        140.8038967
      ],
      
];

/* Not found:
    
    Rauma:
    https://www.raumantaidemuseo.fi/rauman-julkiset-taideteokset/
    https://www.wikiwand.com/fi/Luettelo_Rauman_muistolaatoista
    https://www.wikiwand.com/fi/Luettelo_Rauman_julkisista_taideteoksista_ja_muistomerkeist%C3%A4
    https://www.rauma.fi/asuminen-ja-ymparisto/ymparisto/puistot-ja-metsat/leikkikentat/

    Tampere:
    http://tampereenpatsaat.fi/teokset/

*/